package storage

import "fmt"

type StorageError struct {
	code int
}

func (this *StorageError) Error() string {
	return fmt.Sprintf("StorageError:%v", this.code)
}

func NewStorageError(code int) *StorageError {
	return &StorageError{code}
}

const (
	ErrRetrieveSourceNotFound = 0
	ErrDecryptPassword        = 1
	ErrDecryptGpg             = 2
	ErrExtractCreateDest      = 3
	ErrExtractUnzip           = 4
)

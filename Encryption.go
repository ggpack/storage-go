package storage

import "bytes"
import "io"
import "log"
import "golang.org/x/crypto/openpgp"
import "golang.org/x/crypto/openpgp/packet"

var packetConfig = packet.Config{DefaultCipher: packet.CipherAES128}

// Reads the input `plaintextReader` and encrypts it with `password` based on `openpgp` algo,
// then returns the result as an io.Reader.
// Up to the caller to redirect the output wherever he wants (file, socket, ...).
func Encrypt(plaintextReader io.Reader, password []byte) (io.Reader, error) {
	var encbuf bytes.Buffer
	plaintextWC, _ := openpgp.SymmetricallyEncrypt(&encbuf, password, nil, &packetConfig)

	plainData, err := io.ReadAll(plaintextReader)
	if err != nil {
		log.Print("KO")
		return nil, err
	}
	_, err = plaintextWC.Write(plainData)
	if err != nil {
		log.Print("KO")
		return nil, err
	}

	plaintextWC.Close()

	log.Print("OK Encrypt")
	return &encbuf, nil
}

// Reads the input `cypherTextReader` and decrypts it with `password` based on `openpgp` algo,
// then returns the result as an io.Reader.
// Up to the caller to redirect the output wherever he wants (file, Extract zip, ...).
func Decrypt(ciphertextReader io.Reader, password []byte) (io.Reader, error) {
	log.Print("Decrypt started")
	// Function simulating the password retrieval
	// Will be called again in case of bad password
	alreadyTried := false
	prompt := func(keys []openpgp.Key, symmetric bool) ([]byte, error) {
		if alreadyTried {
			return nil, NewStorageError(ErrDecryptPassword)
		}
		alreadyTried = true
		return password, nil
	}

	msgDetails, err := openpgp.ReadMessage(ciphertextReader, nil, prompt, &packetConfig)
	if err != nil {
		return nil, NewStorageError(ErrDecryptGpg)
	}

	log.Print("OK Decrypt")
	return msgDetails.UnverifiedBody, nil
}

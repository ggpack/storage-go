package storage

import "io"
import "os"

// Flexible way to pass params to the constructor
type Params map[string]any

func (this Params) Get(key string, defaultVal any) any {
	if value, exists := this[key]; exists {
		return value
	} else {
		return defaultVal
	}
}

// Object owner of the encryption and compression options.
// Can be global (application wise) or contextual (session/user wise).
type Storage struct {
	password          []byte
	includeRootFolder bool
}

func NewStorage(args Params) *Storage {
	return &Storage{
		password:          args.Get("password", []byte{}).([]byte),
		includeRootFolder: args.Get("includeRootFolder", false).(bool),
	}
}

// Entrypoint to compress and encrypt a directory.
// Equivalent to the shell command:
// `tar cz -C $inputDirPath . | gpg -c --batch --yes --passphrase $aKey -o $outputArchivePath`
func (this *Storage) Archive(inputDirPath string, outputArchivePath string) error {
	plainZip, err := Compress(inputDirPath, this.includeRootFolder)
	if err != nil {
		return err
	}
	ciphertextReader, err := Encrypt(plainZip, this.password)
	if err != nil {
		return err
	}

	ciphertext, err := io.ReadAll(ciphertextReader)
	if err != nil {
		return err
	}

	return os.WriteFile(outputArchivePath, ciphertext, 0644)
}

// Entrypoint to decrypt and extract a directory.
// Equivalent to the shell command:
// `gpg -d --batch --yes --passphrase $aKey $inputArchivePath | tar -C $outputDirPath -xzf -`
func (this *Storage) Retrieve(inputArchivePath string, outputDirPath string) error {
	ciphertextReader, err := os.Open(inputArchivePath)
	if err != nil {
		return NewStorageError(ErrRetrieveSourceNotFound)
	}
	defer ciphertextReader.Close()

	zippedData, err := Decrypt(ciphertextReader, this.password)
	if err != nil {
		return err
	}

	return Extract(zippedData, outputDirPath)
}

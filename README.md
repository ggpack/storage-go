# Storage

[![Pipeline status](https://gitlab.com/ggpack/storage-go/badges/master/pipeline.svg)](https://gitlab.com/ggpack/storage-go/pipelines)
[![Coverage report](https://gitlab.com/ggpack/storage-go/badges/master/coverage.svg?job=test.unittest)](https://gitlab.com/ggpack/storage-go/-/jobs)

[![License](https://img.shields.io/badge/license-ISC-blue.svg)](https://gitlab.com/ggpack/storage-go/-/blob/master/LICENSE)

Compress and encrypt your directories 🗜️🔐

# Principle
2 steps operation to produce an encrypted archive `.enc1` from a directory:
1. archive the origin directory with tar + gzip
1. encrypt the archive with gpg

# Usage

## Initialization
``` go
import "gitlab.com/ggpack/storage-go"

// Using a byte slice
var password []byte
store := storage.NewStorage(password)

// Using a string
var password string
store := storage.NewStorageStr(password)

```

## Make an archive from a directory
``` go
err := store.Archive("originDir", "destinationArchive.enc1")
if err != nil {
    log.Error("Archiving error", err)
}
```

## Extract an archive to a directory
``` go
err := store.Retrieve("originArchive.enc1", "destinationDir")
if err != nil {
    log.Error("Retrieve error: ", err)
}
```
----

# Contributing

## Install
[**The code is hosted on Gitlab 🦊**](https://gitlab.com/ggpack/storage-go)

Simply clone and submit merge requests.

## Testing
The unit tests are located in the `test` folder code and ran by `go.testing`.

``` bash
# Run all
go test ./test
```

## Releasing
The process is triggered by a tag added to a commit. The tag must match the pattern `v<VERSION>`
and `VERSION` has to comply to **[semver](https://semver.org)**.

The package publication is automatically managed by go after the tagging,
you can check it on [pkg.go.dev](https://pkg.go.dev/gitlab.com/ggpack/storage-go) after a few hours.

# Thanks
[Smashicons](https://www.flaticon.com/authors/Smashicons) from [www.flaticon.com](https://www.flaticon.com) for the icon.

module storage/example

go 1.21

require gitlab.com/ggpack/logchain-go v1.1.0
require gitlab.com/ggpack/storage-go v0.0.0

require golang.org/x/crypto v0.14.0 // indirect

replace gitlab.com/ggpack/storage-go => ../

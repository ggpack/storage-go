package main

import "os"

import "gitlab.com/ggpack/logchain-go"
import "gitlab.com/ggpack/storage-go"

// Extension for the encrypted archive.
// First version of the algo, may change in the future.
const kEncryptedExtV1 = ".enc1"

func setupLogging() logchain.Logger {
	params := logchain.Params{
		"sigLength": 8,
		"verbosity": 5,
	}
	chainer := logchain.NewLogChainer(params)
	return chainer.InitLogging()
}

var log = setupLogging()

func internalMain() int {
	password := []byte("the_password")

	store := storage.NewStorage(storage.Params{"password": password})

	err := os.RemoveAll("open") // At this point it could only exist by accident, let's clear
	if err != nil {
		log.Infof("Error: %v\n", err)
	}
	err = os.RemoveAll("open2") // At this point it could only exist by accident, let's clear
	if err != nil {
		log.Infof("Error: %v\n", err)
	}

	err = store.Archive("theDir", "store/theDir.enc1")
	if err != nil {
		log.Error("Archiving error", err)
		return 1
	}
	log.Info("Archiving done")

	err = store.Retrieve("store/theDir.enc1", "open")
	if err != nil {
		log.Error("Retrieve error: ", err)
		return 1
	}
	log.Info("Retrieval done")

	store = storage.NewStorage(storage.Params{"password": password, "includeRootFolder": true})

	err = store.Archive("theDir", "store/theDir2.enc1")
	if err != nil {
		log.Error("Archiving error", err)
		return 1
	}
	log.Info("Archiving done")

	err = store.Retrieve("store/theDir2.enc1", "open2")
	if err != nil {
		log.Error("Retrieve error: ", err)
		return 1
	}
	log.Info("Retrieval done")

	return 0
}

func main() {
	os.Exit(internalMain())
}

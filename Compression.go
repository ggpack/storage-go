// Directory compression/extraction functions
// Adapted from "github.com/walle/targz" (MIT)
package storage

import "archive/tar"
import "bufio"
import "bytes"
import "compress/gzip"
import "errors"
import "io"
import "log"
import "os"
import "path/filepath"

// Compress creates a archive from the folder inputDirPath and returns an io.Reader
// so that the caller can redirect it where he wants (file, socket, ...).
// It only adds the last directory in inputDirPath to the archive, not the whole path.
func Compress(inputDirPath string, includeRootFolder bool) (io.Reader, error) {
	inputDirPath = stripTrailingSlashes(inputDirPath)
	inputDirPath, err := filepath.Abs(inputDirPath)
	if err != nil {
		return nil, err
	}

	var subPath string
	if includeRootFolder {
		subPath = filepath.Dir(inputDirPath)
	} else {
		subPath = inputDirPath
	}

	//log.Println("Compress " + subPath + " " + inputDirPath)

	// The main interaction with tar and gzip. Creates a archive and recursivly adds all files in the directory.
	// According to includeRootFolder, it can add an extra depth with the root folder

	// The finished archive contains just the directory added, not any parents.
	// This is possible by giving the whole path exept the final directory in subPath.

	files, err := os.ReadDir(inputDirPath)
	if err != nil {
		return nil, err
	}

	if len(files) == 0 {
		return nil, errors.New("targz: input directory is empty")
	}

	// Wow, that how it should be.
	var outBuff bytes.Buffer
	gzipWriter := gzip.NewWriter(&outBuff)
	tarWriter := tar.NewWriter(gzipWriter)

	err = writeDirectory(inputDirPath, tarWriter, subPath)
	if err != nil {
		return nil, err
	}

	err = tarWriter.Close()
	if err != nil {
		return nil, err
	}

	err = gzipWriter.Close()
	if err != nil {
		return nil, err
	}

	return &outBuff, nil
}

// Extract reads `zippedData` from the input io.Reader
// and stores it into the directory `outputDirPath`, recreating the tree structure.
func Extract(zippedData io.Reader, outputDirPath string) error {

	log.Println("Extract")
	outputDirPath = stripTrailingSlashes(outputDirPath)

	undoDir, err := mkdirAll(outputDirPath)
	if err != nil {
		return NewStorageError(ErrExtractCreateDest)
	}
	defer func() {
		if err != nil {
			undoDir()
		}
	}()

	// maybe add func io.ReadAll(r io.Reader) ([]byte, error)
	gzipReader, err := gzip.NewReader(zippedData)
	if err != nil {
		return NewStorageError(ErrExtractUnzip)
	}
	defer gzipReader.Close()

	tarReader := tar.NewReader(gzipReader)

	for {
		header, err := tarReader.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			return NewStorageError(ErrExtractUnzip)
		}

		fileInfo := header.FileInfo()

		if fileInfo.Name() == "." {
			log.Print("There is no folder at the root of the archive, continue iterating on the files")
			continue
		}

		dir := filepath.Join(outputDirPath, filepath.Dir(header.Name))
		filename := filepath.Join(dir, fileInfo.Name())

		err = os.MkdirAll(dir, 0755)
		if err != nil {
			return NewStorageError(ErrExtractCreateDest)
		}

		file, err := os.Create(filename)
		if err != nil {
			log.Print("Issue creating ", filename)
			return NewStorageError(ErrExtractCreateDest)
		}

		writer := bufio.NewWriter(file)

		buffer := make([]byte, 4096)
		for {
			n, err := tarReader.Read(buffer)
			if err != nil && err != io.EOF {
				panic(err)
			}
			if n == 0 {
				break
			}

			_, err = writer.Write(buffer[:n])
			if err != nil {
				return NewStorageError(ErrExtractUnzip)
			}
		}

		err = writer.Flush()
		if err != nil {
			return NewStorageError(ErrExtractUnzip)
		}

		err = file.Close()
		if err != nil {
			return NewStorageError(ErrExtractUnzip)
		}
	}

	return nil
}

// Remove up to 1 trailing slash.
func stripTrailingSlashes(path string) string {
	if len(path) > 0 && path[len(path)-1] == '/' {
		path = path[0 : len(path)-1]
	}

	return path
}

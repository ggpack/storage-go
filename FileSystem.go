package storage

import "archive/tar"
import "io"
import "log"
import "os"
import "path"
import "path/filepath"
import "syscall"

// Creates all directories with os.MakedirAll and returns a function to remove the first created directory so cleanup is possible.
func mkdirAll(dirPath string) (func(), error) {
	log.Print("mkdirAll")
	var undoDir string

	for aPath := dirPath; ; aPath = path.Dir(aPath) {
		finfo, err := os.Stat(aPath)

		if err == nil {
			if finfo.IsDir() {
				// From there, the directories exist.
				break
			}

			finfo, err = os.Lstat(aPath)
			if err != nil {
				return nil, err
			}

			if finfo.IsDir() {
				// From there, the path exist, as a symlink to a dir.
				break
			}

			return nil, &os.PathError{Op: "mkdirAll", Path: aPath, Err: syscall.ENOTDIR}
		}

		if os.IsNotExist(err) {
			undoDir = aPath
		} else {
			return nil, err
		}
	}

	if undoDir == "" {
		return func() {}, nil
	}

	if err := os.MkdirAll(dirPath, 0755); err != nil {
		return nil, err
	}

	return func() { os.RemoveAll(undoDir) }, nil
}

// Read a directy and write it to the tar writer. Recursive function that writes all sub folders.
func writeDirectory(directory string, tarWriter *tar.Writer, subPath string) error {
	//log.Print("writeDirectory " + subPath + " : " + directory)
	entries, err := os.ReadDir(directory)
	if err != nil {
		return err
	}

	for _, entry := range entries {
		currentPath := filepath.Join(directory, entry.Name())
		if entry.IsDir() {
			err := writeDirectory(currentPath, tarWriter, subPath)
			if err != nil {
				return err
			}
		} else {
			aFileInfo, _ := entry.Info()
			err = writeTarGz(currentPath, tarWriter, aFileInfo, subPath)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

// Write path without the prefix in subPath to tar writer.
func writeTarGz(path string, tarWriter *tar.Writer, fileInfo os.FileInfo, subPath string) error {

	file, err := os.Open(path)
	if err != nil {
		return err
	}
	defer file.Close()

	evaledPath, err := filepath.EvalSymlinks(path)
	if err != nil {
		return err
	}

	subPath, err = filepath.EvalSymlinks(subPath)
	if err != nil {
		return err
	}

	link := ""
	if evaledPath != path {
		link = evaledPath
	}

	header, err := tar.FileInfoHeader(fileInfo, link)
	if err != nil {
		return err
	}
	header.Name = evaledPath[len(subPath):]
	//log.Print("writeTarGz " + header.Name + " " + subPath)

	err = tarWriter.WriteHeader(header)
	if err != nil {
		return err
	}

	_, err = io.Copy(tarWriter, file)
	if err != nil {
		return err
	}

	return err
}

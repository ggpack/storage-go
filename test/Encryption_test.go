package storagetest

import "gitlab.com/ggpack/storage-go"
import "bytes"
import "strings"
import "testing"

func TestEncryption(t *testing.T) {

	plaintextReader := strings.NewReader("plaintext")
	password := []byte("the_password")
	cyphertextReader, err := storage.Encrypt(plaintextReader, password)

	if err != nil {
		t.Error("An error should not have happened")
	}

	var outBuff bytes.Buffer
	outBuff.ReadFrom(cyphertextReader)

	if len(outBuff.Bytes()) < 50 {
		t.Error("Cyphertext too short")
	}
}

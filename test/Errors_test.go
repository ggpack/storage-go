package storagetest

import "gitlab.com/ggpack/storage-go"
import "testing"

func TestErrorFormatting(t *testing.T) {
	theError := storage.NewStorageError(storage.ErrExtractUnzip)
	theDisplay := theError.Error()

	expectedDisplay := "StorageError:4"
	if theDisplay != expectedDisplay {
		t.Errorf("Error formatting issue, expected: %s, got: %s", theDisplay, expectedDisplay)
	}
}

package storagetest

import "gitlab.com/ggpack/storage-go"
import "testing"
import "log"
import "path"
import "os"

func TestCompressTargetNotExisting(t *testing.T) {
	reader, err := storage.Compress("🥺", true)

	if reader != nil {
		log.Println("TestCompressTargetNotExisting", reader, err)
		t.Errorf("The reader should be nil")
	}

	if err == nil {
		t.Errorf("The error should not be nil")
	}
}

func TestCompressTargetNotDir(t *testing.T) {
	reader, err := storage.Compress("../go.mod", true)

	if reader != nil {
		t.Errorf("The reader should be nil %v", err)
	}

	if err == nil {
		t.Errorf("The error should not be nil")
	}
}

func TestCompressTargetDirEmpty(t *testing.T) {

	inputDirPath := path.Join(rootDataDir, "emptyCompressDir")
	os.MkdirAll(inputDirPath, 0755)
	reader, err := storage.Compress(inputDirPath, false)

	if reader != nil {
		t.Errorf("The reader should be nil")
	}

	if err == nil {
		t.Errorf("The error should not be nil")
	}
}

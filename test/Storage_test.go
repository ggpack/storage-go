package storagetest

import "gitlab.com/ggpack/storage-go"
import "path"
import "os"
import "testing"

const rootDataDir = "/tmp/data"

func createBasicTestData(inputDirPath string, t *testing.T) {

	os.MkdirAll(inputDirPath, 0755)
	file1Path := path.Join(inputDirPath, "file1.txt")
	err := os.WriteFile(file1Path, []byte("Hello Gg"), 0644)

	folderPath := path.Join(inputDirPath, "folder")
	os.MkdirAll(folderPath, 0755)

	file2Path := path.Join(folderPath, "file2.txt")
	err = os.WriteFile(file2Path, []byte("Yolo"), 0644)

	if err != nil {
		t.Fatal("Could not write testing data, got", err)
	}
}

func fsExists(pathToFileOrFolder string) bool {
	_, err := os.Stat(pathToFileOrFolder)
	return !os.IsNotExist(err)
}

func TestStorageEnd2End(t *testing.T) {
	t.Cleanup(func() { os.RemoveAll(rootDataDir) })

	inputDirPath := path.Join(rootDataDir, "test1InDir")
	outputArchivePath := inputDirPath + ".enc1"
	outputRetrieveDirPath := path.Join(rootDataDir, "test1OutDir")
	createBasicTestData(inputDirPath, t)

	outputRetrieveDirPath += "///" // Test the trailing slashes

	store := storage.NewStorage(storage.Params{"password": []byte("the_password")})
	err := store.Archive(inputDirPath, outputArchivePath)
	if err != nil {
		t.Error("No archiving error expected, got", err)
	}
	if !fsExists(outputArchivePath) {
		t.Errorf("The archive file %s should exist.", outputArchivePath)
	}

	err = store.Retrieve(outputArchivePath, outputRetrieveDirPath)
	if err != nil {
		t.Error("No retrieval error expected, got", err)
	}
	if !fsExists(outputRetrieveDirPath) {
		t.Errorf("The unarchived dir %s should exist.", outputRetrieveDirPath)
	}
}

// Does not work on runners using root user.
func TestStorageRetrieveMissingRights(t *testing.T) {

	inputDirPath := path.Join(rootDataDir, "test1InDir")
	outputArchivePath := inputDirPath + ".enc1"
	outputRetrieveDirPath := path.Join(rootDataDir, "test2OutDir")
	createBasicTestData(inputDirPath, t)

	os.MkdirAll(outputRetrieveDirPath, 0500) // r-x does not allow to unarchive there

	store := storage.NewStorage(storage.Params{"password": []byte("the_password")})
	store.Archive(inputDirPath, outputArchivePath)
	err := store.Retrieve(outputArchivePath, outputRetrieveDirPath)

	if err == nil {
		t.Error("An error should have been returned")
	}
	unarchivedDir := path.Join(outputRetrieveDirPath, "test1InDir")
	if fsExists(unarchivedDir) {
		t.Errorf("The unarchived dir %s should not exist.", outputRetrieveDirPath)
	}
}

func TestStorageRetrieveBadArchive(t *testing.T) {
	t.Cleanup(func() { os.RemoveAll(rootDataDir) })
	inputDirPath := path.Join(rootDataDir, "test1InDir")
	outputRetrieveDirPath := path.Join(rootDataDir, "test2OutDir")
	createBasicTestData(inputDirPath, t)

	// This is not an enc1 archive
	outputArchivePath := path.Join(inputDirPath, "file1.txt")

	store := storage.NewStorage(storage.Params{"password": []byte("the_password")})
	err := store.Retrieve(outputArchivePath, outputRetrieveDirPath)

	if err == nil {
		t.Error("An error should have been returned")
	}
	unarchivedDir := path.Join(outputRetrieveDirPath, "test1InDir")
	if fsExists(unarchivedDir) {
		t.Errorf("The unarchived dir %s should not exist.", outputRetrieveDirPath)
	}

	// Simulate a user typo
	err = store.Retrieve("DoesNotExist.enc1", outputRetrieveDirPath)

	if err == nil {
		t.Error("An error should have been returned")
	}
	unarchivedDir = path.Join(outputRetrieveDirPath, "test1InDir")
	if fsExists(unarchivedDir) {
		t.Errorf("The unarchived dir %s should not exist.", outputRetrieveDirPath)
	}
}

func TestStorageRetrieveWrongPassword(t *testing.T) {
	t.Cleanup(func() { os.RemoveAll(rootDataDir) })
	inputDirPath := path.Join(rootDataDir, "test1InDir")
	createBasicTestData(inputDirPath, t)

	outputArchivePath := inputDirPath + ".enc1"
	outputRetrieveDirPath := path.Join(rootDataDir, "test3OutDir")

	store := storage.NewStorage(storage.Params{"password": []byte("the_password")})
	store.Archive(inputDirPath, outputArchivePath)

	store = storage.NewStorage(storage.Params{"password": []byte("wrong_password")})
	err := store.Retrieve(outputArchivePath, outputRetrieveDirPath)

	if err == nil {
		t.Error("An error should have been returned")
	}
	unarchivedDir := path.Join(outputRetrieveDirPath, "test1InDir")
	if fsExists(unarchivedDir) {
		t.Errorf("The unarchived dir %s should not exist.", outputRetrieveDirPath)
	}
}

func TestStorageAlreadyExistingUnarchivedDir(t *testing.T) {
	t.Cleanup(func() { os.RemoveAll(rootDataDir) })
	inputDirPath := path.Join(rootDataDir, "test1InDir")
	createBasicTestData(inputDirPath, t)

	outputArchivePath := inputDirPath + ".enc1"
	outputRetrieveDirPath := path.Join(rootDataDir, "test4OutDir")

	// Create a file to confict with the unarchiving
	// TODO Not only we fail but we screw-up in the middle of the unarchiving tree
	os.MkdirAll(outputRetrieveDirPath, 0755)
	file1Path := path.Join(outputRetrieveDirPath, "test1InDir")
	os.WriteFile(file1Path, []byte("Hey! Do not override me, mofo!"), 0644)

	store := storage.NewStorage(storage.Params{"password": []byte("the_password"), "includeRootFolder": true})
	store.Archive(inputDirPath, outputArchivePath)

	err := store.Retrieve(outputArchivePath, outputRetrieveDirPath)
	if err == nil {
		t.Error("An error should have been returned")
	}

	info, err := os.Stat(file1Path)
	if info.IsDir() {
		t.Errorf("The unarchived dir %s should not exist, there was a file already there.", outputRetrieveDirPath)
	}
}
